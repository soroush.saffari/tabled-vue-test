# Front-end Development Test (Vue)

This project is an exercise for the technical test A15 at Tabled.

### Install Dependencies
1. Install latest version of Node and NPM.
2. Run `npm install`

### Run the App
Run `npm run serve`

- - -

Further instructions to run and complete this exercise will be provided during the interview.